#include "base/Bases.h"
#include "RtmpConfigParser.h"

#include "rtmp.h"

RTMP *rtmp = nullptr;

char *url = nullptr;

pthread_t pidPush = 0;

bool playing = false;
FILE *h264File;
FILE *aacFile;
FILE *flvFile;

AudioSpecificConfig audioSpecificConfig = {};

bool _prepare();

bool _read();

//传统方法保存RTMP为FLV文件
//这种方法是不规范的，本质上是直接保存RTMP流，而不是FLV文件
//因为一些专业的播放器，能够智能识别格式，所以才能正常播放
//如果交给分析工具，或者开发者自己写的播放客户端，大概率是会出问题的
void writeToFlv() {
    //写FLV
    char *buffer = (char *) malloc(1024 * 1024);
    while (playing) {
        int len = RTMP_Read(rtmp, buffer, 1024 * 1024);
        if (len > 0)
            fwrite(buffer, len, 1, flvFile);
    }
    fflush(flvFile);
}

//写FLV-TAG
void writeFlvTag(uint8_t tagType, uint32_t tagSize, uint32_t bodySize, uint32_t timestamp, char *body) {
    unsigned char *pBodySize = (unsigned char *) &bodySize;
    unsigned char *pTimestamp = (unsigned char *) &timestamp;
    unsigned char *pTagSize = (unsigned char *) &tagSize;
    const unsigned char tagHeader[] = {
            tagType, //TAG Type，0x08表示音频，0x09表示视频，0x12表示脚本
            *(pBodySize + 2), *(pBodySize + 1), *pBodySize, //Body Size，按字节拷贝bodySize
            *(pTimestamp + 2), *(pTimestamp + 1), *pTimestamp, //Timestamp，按字节拷贝timestamp
            0x00, //Timestamp Extended，时间戳扩展
            0x00, 0x00, 0x00 //StreamID，永远为0
    };
    const unsigned char tagSizeBytes[] = {
            *(pTagSize + 3), *(pTagSize + 2), *(pTagSize + 1), *pTagSize
    };
    fwrite(tagHeader, 11, 1, flvFile);
    fwrite(body, bodySize, 1, flvFile);
    fwrite(tagSizeBytes, 4, 1, flvFile);
    fflush(flvFile);
}

//JNI接口加载完毕
//System.loadLibrary函数被调用时，会触发此方法
extern "C" JNIEXPORT jint JNICALL
JNI_OnLoad(JavaVM *vm, void *reserved) {
    JNI::jvm = vm;
    //取流的同时，将流媒体数据同时写入三个文件
    //H264视频文件，AAC音频文件，FLV混合文件
    h264File = fopen("sdcard/1.h264", "wb");
    aacFile = fopen("sdcard/1.aac", "wb");
    flvFile = fopen("sdcard/1.flv", "wb");
    return JNI_VERSION_1_6;
}

//JNI接口被JVM回收时，会触发此方法
extern "C" JNIEXPORT void JNICALL
JNI_OnUnload(JavaVM *vm, void *reserved) {
    //删除Java回调
    delete Bases::java;
    Bases::java = nullptr;
}

extern "C"
JNIEXPORT void JNICALL
Java_easing_android_media_RtmpPlayer_RtmpPlayer_native_1initialize(JNIEnv *env, jobject interface, jstring tag,jstring path) {
    //设置日志标签
    JNIPrivate::TAG = JNI::toConstChar(tag);
    //记录JNI环境
    JNI::env = env;
    JNI::interface = env->NewGlobalRef(interface);
    //stdio重定向到logcat
    JNI::stdioToLogcat();
    //创建JavaCaller，用于回调Java层方法
    Bases::java = new JavaCaller();
    std::cout << "initialized" << std::endl;
}

extern "C"
JNIEXPORT void JNICALL
Java_easing_android_media_RtmpPlayer_RtmpPlayer_native_1prepare(JNIEnv *env, jobject interface, jstring jUrl) {
    //jstring地址转char*地址
    const char *constUrl = JNI::toConstChar(jUrl);
    url = JNI::toChar(constUrl);
    //准备播放环境
    playing = false;
    bool ret = _prepare();
    if (!ret) {
        std::cout << "prepare fail" << std::endl;
        return;
    }
    playing = true;
    //创建推流线程
    pthread_create(&pidPush, nullptr, [](void *context) -> void * {
        _read();
        return nullptr;
    }, nullptr);
}

extern "C"
JNIEXPORT void JNICALL
Java_easing_android_media_RtmpPlayer_RtmpPlayer_native_1release(JNIEnv *env, jobject interface) {
    playing = false;
    //等待推流结束
    pthread_join(pidPush, nullptr);
    //销毁RTMP
    if (rtmp) {
        RTMP_Close(rtmp);
        RTMP_Free(rtmp);
    }
    //销毁URL
    delete (url);
}

//连接服务器
bool _prepare() {

    //连接RTMP
    rtmp = RTMP_Alloc();
    RTMP_Init(rtmp);
    rtmp->Link.timeout = 30;
    RTMP_SetupURL(rtmp, url);
    rtmp->Link.lFlags |= RTMP_LF_LIVE;
    RTMP_SetBufferMS(rtmp, 1 * 1000);
    RTMP_Connect(rtmp, nullptr);
    RTMP_ConnectStream(rtmp, 0);

    return true;
}

//读取流数据，并保存为H264和AAC
bool _read() {
    bool ret = true;
    RTMPPacket packet = {};

    //写FLV文件头
    const char flvHeader[] = {
            'F', 'L', 'V', //固定签名
            0x01, //FLV版本号
            0x05, //数据类型，0x01表示视频，0x04表示音频，0x05表示音视频都有
            0x00, 0x00, 0x00, 0x09, //FLV头长度
            0x00, 0x00, 0x00, 0x00 //首个TAG前的固定Size，永远为0
    };
    fwrite(flvHeader, 13, 1, flvFile);
    fflush(flvFile);

    //循环读取流数据
    while (playing) {
        ret = RTMP_ReadPacket(rtmp, &packet);
        if (!ret) continue;
        ret = RTMPPacket_IsReady(&packet);
        if (!ret) continue;
        if (!packet.m_nBodySize) continue;

        //读取包信息
        uint8_t headerType = packet.m_headerType;
        uint8_t tagType = packet.m_packetType;
        uint32_t bodySize = packet.m_nBodySize;
        uint32_t tagSize = bodySize + 11;
        uint32_t timestamp = packet.m_nTimeStamp;
        char *body = packet.m_body;

        //处理脚本包
        if (packet.m_packetType == RTMP_PACKET_TYPE_INFO && RTMP_ClientPacket(rtmp, &packet)) {
            //写入FLV文件
            writeFlvTag(0x12, tagSize, bodySize, timestamp, body);
            //解析媒体参数
            MediaMetadata metadata = {};
            parseScriptTag(body, bodySize, metadata);
            std::string strMetadata;
            metadata.toString(strMetadata);
            std::cout << "Media Metadata" << std::endl << strMetadata << std::endl;
            //执行Java回调，通知媒体参数
            Bases::java->onAudioParamChange(metadata.audioSampleRate, metadata.audioSampleSize, metadata.audioChannelStereo == 0 ? 1 : 2);
            Bases::java->onVideoParamChange(metadata.videoWidth, metadata.videoHeight, metadata.videoFrameRate);
        }

        //处理音频包
        if (packet.m_packetType == RTMP_PACKET_TYPE_AUDIO && RTMP_ClientPacket(rtmp, &packet)) {
            //写入FLV文件
            writeFlvTag(0x08, tagSize, bodySize, timestamp, body);
            //解析参数
            unsigned short header1 = (body[0] & 0xF0) >> 4; //前4位表示音频格式，1表示PCM，2表示MP3，10表示AAC
            unsigned short header2 = body[1]; //0x00表示AAC解码配置，0x01表示AAC裸数据
            bool isAAC = header1 == 0x0A;
            bool isConfigPacket = header2 == 0x00;
            //不是AAC
            if (!isAAC) {
                std::cout << "audio format is not aac " << header1 << std::endl;
                continue;
            }
            //解包AAC数据，存入AAC文件
            if (isConfigPacket) {
                //从RTMPPacketBody中解析出音频参数
                AudioParam audioParam = {};
                audioParam.format = (body[0] & 0xF0) >> 4; //音频格式，1表示PCM，2表示MP3，10表示AAC
                audioParam.sampleRate = (body[0] & 0x0C) >> 2; //音频采样率，1表示11KHz，2表示22kHz，3表示44kHz
                audioParam.bitDepth = (body[0] & 0x02) >> 1; //音频采样精度，0表示8位，1表示16位
                audioParam.channels = body[0] & 0x01; //0表示单声道，1表示多声道
                audioParam.packetType = body[1]; //0表示音频配置包，1表示音频裸数据
                audioParam.audioSpecificConfig = ((body[2] & 0xFF) << 8) + (0x00FF & body[3]);
                //解析自定义部分数据
                int aotSize = packet.m_nBodySize - 4;
                uint8_t *aotSpecificConfig = (uint8_t *) malloc(aotSize);
                memcpy(aotSpecificConfig, body + 4, aotSize);
                audioParam.aotSpecificConfig = aotSpecificConfig;
                //提取AudioSpecificConfig，用于生成ADTS（AAC头信息）
                //想要理解此部分代码，可以找个标准的FLV文件
                //然后用FLVAnalyzer软件解析下包结构，对照解析结果逐个字节理解即可
                audioSpecificConfig.audioObjectType = (audioParam.audioSpecificConfig & 0xF800) >> 11; //AudioObjectType占5位
                audioSpecificConfig.sampleFrequencyIndex = (audioParam.audioSpecificConfig & 0x0780) >> 7; //SampleFrequencyIndex占4位
                audioSpecificConfig.channelConfiguration = (audioParam.audioSpecificConfig & 0x78) >> 3; //ChannelConfiguration占4位
                audioSpecificConfig.frameLengthFlag = (audioParam.audioSpecificConfig & 0x04) >> 2; //FrameLengthFlag占1位
                audioSpecificConfig.dependOnCoreCoder = (audioParam.audioSpecificConfig & 0x02) >> 1; //DependOnCoreCoder占1位
                audioSpecificConfig.extensionFlag = audioParam.audioSpecificConfig & 0x01; //ExtensionFlag占1位
            } else {
                //创建ADTS
                char adts[7] = {};
                createADTS(audioSpecificConfig, packet.m_nBodySize - 2 + 7, adts); //除掉AudioBody的前两个参数字节，再加上ADTS的7个字节
                //写入ADTS到AAC文件
                fwrite(adts, 7, 1, aacFile);
                fflush(aacFile);
                //写入AAC裸数据到AAC文件
                fwrite(body + 2, bodySize - 2, 1, aacFile);
                fflush(aacFile);
                //回调AAC数据给JAVA
                uint8_t *buffer = (uint8_t *) malloc(7 + bodySize - 2);
                memcpy(buffer, adts, 7);
                memcpy(buffer + 7, body + 2, bodySize - 2);
                Bases::java->onAudioData(buffer, 0, 7 + bodySize - 2);
            }
        }

        //处理视频包
        if (packet.m_packetType == RTMP_PACKET_TYPE_VIDEO && RTMP_ClientPacket(rtmp, &packet)) {
            //写入FLV文件
            writeFlvTag(0x09, tagSize, bodySize, timestamp, body);
            //解析参数
            unsigned short header1 = body[0]; //高4位表示帧类型，低4位表示解码器，0x10表示关键帧，0x20表示参照帧，0x07表示AVC
            unsigned short header2 = body[1]; //0x00表示AVC解码配置，0x01表示AVC裸数据
            bool isAvc = (header1 & 0x0F) == 0x07;
            bool isConfigPacket = header2 == 0x00;
            //不是AVC
            if (!isAvc) {
                std::cout << "video format is not avc " << header1 << std::endl;
                continue;
            }
            //H264固定起始码
            const char start_code[4] = {0x00, 0x00, 0x00, 0x01};
            //解包H264数据，存入H264文件
            if (isConfigPacket) {
                //AVC-Header
                //从RTMPPacket中解析SPS
                int sps_num = body[10] & 0x1F;
                int sps_index = 11;
                while (sps_index <= 10 + sps_num) {
                    int sps_len = (body[sps_index] & 0xFF) << 8 | (body[sps_index + 1] & 0xFF);
                    sps_index += 2;
                    //写入H264文件
                    fwrite(start_code, 1, 4, h264File);
                    fwrite(body + sps_index, 1, sps_len, h264File);
                    fflush(h264File);
                    //回调H264数据给JAVA
                    uint8_t *buffer = (uint8_t *) malloc(4 + sps_len);
                    memcpy(buffer, start_code, 4);
                    memcpy(buffer + 4, body + sps_index, sps_len);
                    Bases::java->onVideoData(buffer, 0, 4 + sps_len);
                    //移动到下个SPS
                    sps_index += sps_len;
                }
                //从RTMPPacket中解析PPS
                int pps_num = body[sps_index] & 0x1F;
                int pps_index = sps_index + 1;
                while (pps_index <= sps_index + pps_num) {
                    int pps_len = (body[pps_index] & 0xFF) << 8 | body[pps_index + 1] & 0xFF;
                    pps_index += 2;
                    //写入H264文件
                    fwrite(start_code, 1, 4, h264File);
                    fwrite(body + pps_index, 1, pps_len, h264File);
                    fflush(h264File);
                    //回调H264数据给JAVA
                    uint8_t *buffer = (uint8_t *) malloc(4 + pps_len);
                    memcpy(buffer, start_code, 4);
                    memcpy(buffer + 4, body + pps_index, pps_len);
                    Bases::java->onVideoData(buffer, 0, 4 + pps_len);
                    //移动到下个PPS
                    pps_index += pps_len;
                }
            } else {
                //AVC-NALU
                int nalu_index = 5;
                while (nalu_index < packet.m_nBodySize) {
                    int nalu_len = (body[nalu_index] & 0x000000FF) << 24 | (body[nalu_index + 1] & 0x000000FF) << 16 | (body[nalu_index + 2] & 0x000000FF) << 8 | body[nalu_index + 3] & 0x000000FF;
                    nalu_index = nalu_index + 4;
                    //写入H264文件
                    fwrite(start_code, 1, 4, h264File);
                    fwrite(body + nalu_index, 1, nalu_len, h264File);
                    fflush(h264File);
                    //回调H264数据给JAVA
                    uint8_t *buffer = (uint8_t *) malloc(4 + nalu_len);
                    memcpy(buffer, start_code, 4);
                    memcpy(buffer + 4, body + nalu_index, nalu_len);
                    Bases::java->onVideoData(buffer, 0, 4 + nalu_len);
                    //移动到下个NALU
                    nalu_index += nalu_len;
                }
            }
        }

        //释放Packet
        RTMPPacket_Free(&packet);
    }
    return ret;
}

