#include <iostream>
#include <string.h>
#include <stdio.h>
#include <sstream>

#include "rtmp/rtmp_sys.h"
#include "rtmp/log.h"

struct AudioParam {
    uint8_t format;
    uint8_t sampleRate;
    uint8_t bitDepth;
    uint8_t channels;
    uint8_t packetType;
    uint16_t audioSpecificConfig;
    uint8_t * aotSpecificConfig; //可变部分，用于存储自定义数据，长度为FlvTagSize-固定部分长度
};

struct AudioSpecificConfig {
    uint8_t audioObjectType;
    uint8_t sampleFrequencyIndex;
    uint8_t channelConfiguration;
    uint8_t frameLengthFlag;
    uint8_t dependOnCoreCoder;
    uint8_t extensionFlag;
    uint8_t *AOTSpecificConfig = nullptr; //长度可变，DataSize-固定部分长度，就是这部分的长度
};

struct MediaMetadata {

    public:

    int audioCodecId = 0; //音频解码器
    int audioSampleRate = 44100; //音频采样率
    int audioSampleSize = 16; //音频采样精度
    int audioChannelStereo = 1; //音频声道，0单声道，1多声道
    int videoCodecId = 0; //视频解码器
    int videoWidth = 800; //视频宽度
    int videoHeight = 600; //视频高度
    int videoFrameRate = 30; //视频帧率
    int fileSize = 0; //文件大小

    void toString(std::string &str) {
        std::ostringstream os;
        os << "audioCodecId" << ":" << audioCodecId << std::endl;
        os << "audioSampleRate" << ":" << audioSampleRate << std::endl;
        os << "audioSampleSize" << ":" << audioSampleSize << std::endl;
        os << "audioChannelStereo" << ":" << audioChannelStereo << std::endl;
        os << "videoCodecId" << ":" << videoCodecId << std::endl;
        os << "videoWidth" << ":" << videoWidth << std::endl;
        os << "videoHeight" << ":" << videoHeight << std::endl;
        os << "videoFrameRate" << ":" << videoFrameRate << std::endl;
        os << "fileSize" << ":" << fileSize << std::endl;
        str = os.str();
    }
};

void copyProperty(MediaMetadata &metadata, AMFObjectProperty *property);

//根据AudioSpecificConfig生成ADTS
//FrameSize=ADTS+AACRaw，即Header+Body，整帧长度
void createADTS(AudioSpecificConfig audioSpecificConfig, unsigned short frameSize, char *dst) {
    uint8_t adts[7] = {};
    //Syncword，12位，固定值0xFFF
    adts[0] = 0xFF;
    adts[1] = 0xF0;
    //ID，1位，0表示MPEG4，1表示MPEG2
    adts[1] |= 0x00;
    //Layer，2位，永远是0
    adts[1] |= 0x00;
    //ProtectionAbsent，1位，是否CRC校验，0表示不校验，1表示校验
    adts[1] |= 0x01;
    //Profile，2位，等于audioObjectType-1
    adts[2] = (audioSpecificConfig.audioObjectType - 1) << 6;
    //SampleFrequencyIndex，4位，采样率，4表示44kHz
    adts[2] |= (audioSpecificConfig.sampleFrequencyIndex & 0x0F) << 2;
    //PrivateBit，1位，私有位
    adts[2] |= (0 << 1);
    //ChannelConfiguration，3位，声道数
    adts[2] |= (audioSpecificConfig.channelConfiguration & 0B0100) >> 2;
    adts[3] = (audioSpecificConfig.channelConfiguration & 0B0011) << 6;
    //OriginalCopy，1位
    adts[3] |= (0 << 5);
    //Home，1位
    adts[3] |= (0 << 4);
    //CopyrightedIdBit，1位，永远是0
    adts[3] |= (0 << 3);
    //CopyrightedIdStart，1位，永远是0
    adts[3] |= (0 << 2);
    //AACFrameLength，13位，ADTS和裸数据总长度
    adts[3] |= (frameSize & 0x1800) >> 11;
    adts[4] = (frameSize & 0x07F8) >> 3;
    adts[5] = (frameSize & 0x07) << 5;
    //ADTSBufferFullness，11位，永远是0x7FF，表示码率可变
    adts[5] |= 0x1F;
    adts[6] = 0xFC;
    //RawDataBlockNumber，2位，表示该包中包含多少个原始帧，填0即可
    adts[6] |= 0B0000;
    memcpy(dst, adts, 7);
}

//解析ScriptTag
void parseScriptTag(char *body, int bodySize, MediaMetadata &metadata) {
    //解析AMFObject
    AMFObject obj;
    AMF_Decode(&obj, body, bodySize, false);
    AMF_Dump(&obj);

    //遍历所有属性
    for (int i = 0; i < obj.o_num; i++) {
        AMFObjectProperty *property = AMF_GetProp(&obj, NULL, i);
        if (!property)
            continue;
        //普通属性
        if (property->p_type != AMF_OBJECT) {
            copyProperty(metadata, property);
            continue;
        }
        //嵌套属性
        AMFObject subObject;
        AMFProp_GetObject(property, &subObject);
        for (int m = 0; m < subObject.o_num; m++) {
            AMFObjectProperty *subProperty = AMF_GetProp(&subObject, NULL, m);
            copyProperty(metadata, subProperty);
        }
    }
}

//拷贝Script属性到Metadata
void copyProperty(MediaMetadata &metadata, AMFObjectProperty *property) {
    if (!property || !property->p_name.av_val)
        return;
    //去除属性名后面的特殊字符
    AMFDataType propertyType = property->p_type;
    std::string strPropertyName = property->p_name.av_val;
    strPropertyName.erase(remove_if(strPropertyName.begin(), strPropertyName.end(), [](char c) -> bool {
        return c == '\U00000002';
    }), strPropertyName.end());
    const char *propertyName = strPropertyName.c_str();
    std::cout << "get property: " << propertyName << std::endl;
    //获取属性值
    if (strcmp("stereo", propertyName) == 0) {
        bool value = AMFProp_GetBoolean(property);
        metadata.audioChannelStereo = value;
    } else if (strcmp("width", propertyName) == 0) {
        double value = AMFProp_GetNumber(property);
        metadata.videoWidth = value;
    } else if (strcmp("height", propertyName) == 0) {
        double value = AMFProp_GetNumber(property);
        metadata.videoHeight = value;
    } else if (strcmp("framerate", propertyName) == 0) {
        double value = AMFProp_GetNumber(property);
        metadata.videoFrameRate = value;
    } else if (strcmp("videocodecid", propertyName) == 0) {
        double value = AMFProp_GetNumber(property);
        metadata.videoCodecId = value;
    } else if (strcmp("audiosamplerate", propertyName) == 0) {
        double value = AMFProp_GetNumber(property);
        metadata.audioSampleRate = value;
    } else if (strcmp("audiosamplesize", propertyName) == 0) {
        double value = AMFProp_GetNumber(property);
        metadata.audioSampleSize = value;
    } else if (strcmp("audiocodecid", propertyName) == 0) {
        double value = AMFProp_GetNumber(property);
        metadata.audioCodecId = value;
    } else if (strcmp("filesize", propertyName) == 0) {
        double value = AMFProp_GetNumber(property);
        metadata.fileSize = value;
    }
}




