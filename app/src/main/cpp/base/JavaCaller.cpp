#include "JavaCaller.h"

jmethodID onAudioParamChangeMethod;
jmethodID onVideoParamChangeMethod;
jmethodID onAudioDataMethod;
jmethodID onVideoDataMethod;

//native层回调java方法的工具类
JavaCaller::JavaCaller() {
    //获取Java对象中的回调方法
    jclass clazz = JNI::env->GetObjectClass(JNI::interface);
    onAudioParamChangeMethod = JNI::env->GetMethodID(clazz, "onAudioParamChange", "(III)V");
    onVideoParamChangeMethod = JNI::env->GetMethodID(clazz, "onVideoParamChange", "(III)V");
    onAudioDataMethod = JNI::env->GetMethodID(clazz, "onAudioData", "([BII)V");
    onVideoDataMethod = JNI::env->GetMethodID(clazz, "onVideoData", "([BII)V");
}

//JNI接口对象释放，JavaCaller析构
JavaCaller::~JavaCaller() {
    JNI::env->DeleteGlobalRef(JNI::interface);
    JNI::jvm = nullptr;
    JNI::env = nullptr;
    JNI::interface = nullptr;
}

//音频参数改变
void JavaCaller::onAudioParamChange(uint32_t sampleRate, uint32_t sampleBit, uint32_t channelNum) {
    JNIEnv *env = nullptr;
    bool detached = JNI::jvm->GetEnv((void **) &env, JNI_VERSION_1_6) == JNI_EDETACHED;
    if (detached) JNI::jvm->AttachCurrentThread(&env, nullptr);
    env->CallVoidMethod(JNI::interface, onAudioParamChangeMethod, (jint) sampleRate, (jint) sampleBit, (jint) channelNum);
    if (detached) JNI::jvm->DetachCurrentThread();
}

//视频参数改变
void JavaCaller::onVideoParamChange(uint32_t width, uint32_t height, uint32_t fps) {
    JNIEnv *env = nullptr;
    bool detached = JNI::jvm->GetEnv((void **) &env, JNI_VERSION_1_6) == JNI_EDETACHED;
    if (detached) JNI::jvm->AttachCurrentThread(&env, nullptr);
    env->CallVoidMethod(JNI::interface, onVideoParamChangeMethod, (jint) width, (jint) height, (jint) fps);
    if (detached) JNI::jvm->DetachCurrentThread();
}

//收到音频数据
void JavaCaller::onAudioData(uint8_t *buffer, uint32_t offset, uint32_t length) {
    JNIEnv *env = nullptr;
    bool detached = JNI::jvm->GetEnv((void **) &env, JNI_VERSION_1_6) == JNI_EDETACHED;
    if (detached) JNI::jvm->AttachCurrentThread(&env, nullptr);
    jbyteArray jbyteArray = env->NewByteArray(length);
    env->SetByteArrayRegion(jbyteArray, offset, length, (jbyte *) buffer);
    env->CallVoidMethod(JNI::interface, onAudioDataMethod, jbyteArray, (jint) offset, (jint) length);
    if (detached) JNI::jvm->DetachCurrentThread();
}

//收到视频数据
void JavaCaller::onVideoData(uint8_t *buffer, uint32_t offset, uint32_t length) {
    JNIEnv *env = nullptr;
    bool detached = JNI::jvm->GetEnv((void **) &env, JNI_VERSION_1_6) == JNI_EDETACHED;
    if (detached) JNI::jvm->AttachCurrentThread(&env, nullptr);
    jbyteArray jbyteArray = env->NewByteArray(length);
    env->SetByteArrayRegion(jbyteArray, offset, length, (jbyte *) buffer);
    env->CallVoidMethod(JNI::interface, onVideoDataMethod, jbyteArray, (jint) offset, (jint) length);
    if (detached) JNI::jvm->DetachCurrentThread();
}


