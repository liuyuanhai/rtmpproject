#ifndef JAVA_CALLER
#define JAVA_CALLER

#include "../commons/JniMacro.h"

class JavaCaller {

    public:

    JavaCaller();

    ~JavaCaller();

    //音频参数改变
    void onAudioParamChange(uint32_t sampleRate, uint32_t sampleBit, uint32_t channelNum);

    //视频参数改变
    void onVideoParamChange(uint32_t width, uint32_t height, uint32_t fps);

    //收到音频数据
    void onAudioData(uint8_t *buffer, uint32_t offset, uint32_t length);

    //收到视频数据
    void onVideoData(uint8_t *buffer, uint32_t offset, uint32_t length);
};

#endif

