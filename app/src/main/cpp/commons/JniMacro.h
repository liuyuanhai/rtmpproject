#ifndef JNI_MACRO
#define JNI_MACRO

#include <jni.h>
#include <android/log.h>
#include <stdio.h>
#include <iostream>
#include <unistd.h>
#include <inttypes.h>
#include <pthread.h>

//定义删除指针的宏
#define DELETE(p) if(p){ delete p; p = nullptr; }

//定义Logcat日志打印的宏
#define LOGI(...) __android_log_print(ANDROID_LOG_INFO,JNIPrivate::TAG,__VA_ARGS__)
#define LOGD(...) __android_log_print(ANDROID_LOG_DEBUG,JNIPrivate::TAG,__VA_ARGS__)
#define LOGW(...) __android_log_print(ANDROID_LOG_WARN,JNIPrivate::TAG,__VA_ARGS__)
#define LOGE(...) __android_log_print(ANDROID_LOG_ERROR,JNIPrivate::TAG,__VA_ARGS__)

//定义JniMacro用到的私有变量
namespace JNIPrivate {
    extern const char *TAG;
    extern int fds[2];
    extern pthread_t tid;

    //不断从FD中读取数据并打印
    extern void *pThreadReadStdIO(void *);
}

//定义JNI常用的变量
namespace JNI {
    extern JavaVM *jvm; //Java虚拟机对象
    extern JNIEnv *env; //JNI接口加载时的线程运行环境
    extern jobject interface; //JNI接口对应的Java对象

    //开启JNI日志服务
    extern void stdioToLogcat();

    //char数组转jstring
    extern void toJString(const char *charArray, jstring &string);

    //jstring转char数组
    extern const char *toConstChar(jstring jstring);

    //char*转const char*
    extern const char *toConstChar(char *charArray);

    //const char*转char*
    extern char *toChar(const char *constCharArray);
}

#endif

