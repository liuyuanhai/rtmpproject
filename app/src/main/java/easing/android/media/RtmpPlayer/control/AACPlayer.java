package easing.android.media.RtmpPlayer.control;

import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.media.MediaCodec;
import android.media.MediaCodecInfo;
import android.media.MediaFormat;

import java.io.IOException;
import java.nio.ByteBuffer;

import lombok.SneakyThrows;

@SuppressWarnings("all")
public class AACPlayer {

    AudioTrack audioTrack;
    MediaCodec mediaCodec;

    //代码是写死的，并未使用这些参数
    //实际应用中，请根据字段值来初始化AudioTrack和MediaCodec
    int sampleRate = 44100;
    int sampleBit = 16;
    int channelNum = 2;

    boolean playing = false;

    //设置音频参数
    public void setAudioInfo(int sampleRate, int sampleBit, int channelNum) {
        this.sampleRate = sampleRate;
        this.sampleBit = sampleBit;
        this.channelNum = channelNum;
    }

    @SneakyThrows
    public void start() {
        if (playing)
            return;
        //一次解码的数量
        int inputBufferSize = AudioTrack.getMinBufferSize(44100, AudioFormat.CHANNEL_OUT_STEREO, AudioFormat.ENCODING_PCM_16BIT);
        //启动AudioTrack
        audioTrack = new AudioTrack(AudioManager.STREAM_MUSIC, 44100, AudioFormat.CHANNEL_OUT_STEREO, AudioFormat.ENCODING_PCM_16BIT, inputBufferSize * 4, AudioTrack.MODE_STREAM);
        audioTrack.play();
        //启动MediaCodec
        try {
            mediaCodec = MediaCodec.createDecoderByType("audio/mp4a-latm");
        } catch (IOException e) {
            e.printStackTrace();
        }
        MediaFormat mediaFormat = new MediaFormat();
        mediaFormat.setString(MediaFormat.KEY_MIME, "audio/mp4a-latm");
        mediaFormat.setInteger(MediaFormat.KEY_CHANNEL_COUNT, 2);
        mediaFormat.setInteger(MediaFormat.KEY_SAMPLE_RATE, 44100);
        mediaFormat.setInteger(MediaFormat.KEY_BIT_RATE, 44100 * 16 * 2);
        mediaFormat.setInteger(MediaFormat.KEY_MAX_INPUT_SIZE, inputBufferSize * 4);
        mediaFormat.setInteger(MediaFormat.KEY_IS_ADTS, 1);
        mediaFormat.setInteger(MediaFormat.KEY_AAC_PROFILE, MediaCodecInfo.CodecProfileLevel.AACObjectLC);
        //csd-0存储的是音频参数，csd-0占两个字节，格式为：
        //AAC Profile占5位，Sample Rate占4位，Channel Num占4位，最后3位用0补位
        //AAC Profile取值：AAC-Main 0x01，AAC-LC 0x02，AAC-SSR 0x03
        //Sample Rate取值：44100Hz 0x04，48000Hz 0x03
        //Channel Num取值：单声道 0x01，双声道 0x02
        ByteBuffer csd_0 = ByteBuffer.wrap(new byte[]{0x12, 0x10});
        mediaFormat.setByteBuffer("csd-0", csd_0);
        mediaCodec.configure(mediaFormat, null, null, 0);
        mediaCodec.start();
        pts = 0L;
        playing = true;
    }

    @SneakyThrows
    public void stop() {
        if (!playing)
            return;
        playing = false;
        mediaCodec.stop();
        mediaCodec.release();
        mediaCodec = null;
        audioTrack.stop();
        audioTrack.release();
        audioTrack = null;
    }

    //解码帧数据到Surface
    public boolean decodeFrame(byte[] buffer, int offset, int length) {

        if (!playing)
            return false;

        //input buffer data
        ByteBuffer[] inputBuffers = mediaCodec.getInputBuffers();
        int inputBufferIndex = mediaCodec.dequeueInputBuffer(1000);
        if (inputBufferIndex >= 0) {
            ByteBuffer inputBuffer = inputBuffers[inputBufferIndex];
            inputBuffer.clear();
            inputBuffer.put(buffer, offset, length);
            mediaCodec.queueInputBuffer(inputBufferIndex, 0, length, getPTS(), 0);
        }

        //output decoded data
        ByteBuffer[] outputBuffers = mediaCodec.getOutputBuffers();
        MediaCodec.BufferInfo info = new MediaCodec.BufferInfo();
        int outputBufferIndex = mediaCodec.dequeueOutputBuffer(info, 1000);
        while (outputBufferIndex >= 0) {
            ByteBuffer outputBuffer = outputBuffers[outputBufferIndex];
            byte[] decodeBuffer = new byte[info.size];
            outputBuffer.get(decodeBuffer);
            outputBuffer.clear();
            audioTrack.write(decodeBuffer, offset, info.size);
            mediaCodec.releaseOutputBuffer(outputBufferIndex, false);
            outputBufferIndex = mediaCodec.dequeueOutputBuffer(info, 1000);
        }
        return true;
    }

    long pts = 0L;

    protected long getPTS() {
        long dt = System.nanoTime() / 1000L - pts;
        return dt;
    }
}

