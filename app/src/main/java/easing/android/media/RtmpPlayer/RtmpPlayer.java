package easing.android.media.RtmpPlayer;

import easing.android.media.RtmpPlayer.util.TaskQueue;

@SuppressWarnings("all")
public class RtmpPlayer {

    static {
        System.loadLibrary("RtmpPlayer");
    }

    public final TaskQueue taskQueue = TaskQueue.get();

    //********************************************  Java接口  ********************************************

    //初始化
    public void initialize(String path) {
        String tag = RtmpPlayer.class.getSimpleName();
        native_initialize(tag,path);
    }

    //创建推流器
    public void prepare(String url) {
        taskQueue.add(() -> {
            native_prepare(url);
        });
    }

    //释放推流器
    public void release() {
        taskQueue.add(() -> {
            native_release();
        });
    }

    //********************************************  Java回调  ********************************************

    public interface EventListener {

        default void onAudioParamChange(int sampleRate, int sampleBit, int channelNum) {}

        default void onVideoParamChange(int width, int height, int fps) {}

        default void onAudioData(byte[] buffer, int offset, int length) {}

        default void onVideoData(byte[] buffer, int offset, int length) {}
    }

    EventListener listener = new EventListener() {};

    public RtmpPlayer listener(EventListener listener) {
        this.listener = listener;
        return this;
    }

    //********************************************  Native接口  ********************************************

    native void native_initialize(String tag,String path);

    native void native_prepare(String url);

    native void native_release();

    //********************************************  Native回调  ********************************************

    //音频参数改变
    void onAudioParamChange(int sampleRate, int sampleBit, int channelNum) {
        listener.onAudioParamChange(sampleRate, sampleBit, channelNum);
    }

    //视频参数改变
    void onVideoParamChange(int width, int height, int fps) {
        listener.onVideoParamChange(width, height, fps);
    }

    //收到音频数据
    void onAudioData(byte[] buffer, int offset, int length) {
        listener.onAudioData(buffer, offset, length);
    }

    //收到视频数据
    void onVideoData(byte[] buffer, int offset, int length) {
        listener.onVideoData(buffer, offset, length);
    }
}

