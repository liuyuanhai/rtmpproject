package easing.android.media.RtmpPlayer;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import java.io.File;
import java.io.FileOutputStream;

import butterknife.BindView;
import butterknife.ButterKnife;
import easing.android.media.R;
import easing.android.media.RtmpPlayer.control.AACPlayer;
import easing.android.media.RtmpPlayer.control.H264PlayView;
import easing.android.media.RtmpPlayer.util.CameraUtils;
import easing.android.media.YuvSaver;

//这是一个简单的演示Demo
//使用前请授予完整的存储卡访问权限
@SuppressWarnings("all")
public class PlayActivity extends AppCompatActivity {

    @BindView(R.id.h264PlayView)
    H264PlayView h264PlayView;

    AACPlayer aacPlayer = new AACPlayer();

    RtmpPlayer player = new RtmpPlayer();
    private boolean isgetFrame;//是否得到一帧的数据
    String url;
    YuvSaver yuvSaver;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play);
        ButterKnife.bind(this, PlayActivity.this);
        url = getIntent().getStringExtra("url");
        yuvSaver = new YuvSaver();
        //设置拉流回调
        player.listener = new RtmpPlayer.EventListener() {

            @Override
            public void onAudioParamChange(int sampleRate, int sampleBit, int channelNum) {
                aacPlayer.stop();
                aacPlayer.setAudioInfo(sampleRate, sampleBit, channelNum);
                aacPlayer.start();
            }

            @Override
            public void onVideoParamChange(int width, int height, int fps) {
                h264PlayView.stop();
                h264PlayView.setVideoInfo(width, height, fps);
                h264PlayView.start();
                yuvSaver.setRunning(true);

            }

            @Override
            public void onAudioData(byte[] buffer, int offset, int length) {
                Log.e("---------------->","onAudioData");
                if (!isFinishing())
                    aacPlayer.decodeFrame(buffer, offset, length);
            }

            @Override
            public void onVideoData(byte[] buffer, int offset, int length) {
                Log.e("---------------->","onVideoData");
                if (!isFinishing()){
                    h264PlayView.decodeFrame(buffer, offset, length);
                    yuvSaver.onVideoFrame(buffer);
                }
            }
        };

        //播放器初始化回调
        h264PlayView.onSurfaceChange(() -> {
            //开始拉流播放
            player.initialize(Environment.getExternalStorageDirectory().getAbsolutePath());
            player.prepare(url);
            Log.e("---------------->","onSurfaceChange 开始拉流");

        });
        h264PlayView.setCallback(new H264PlayView.VideoDecoderCallback() {
            @Override
            public void onFrameDecoded(byte[] frameData) {
                if(!isgetFrame){
                    isgetFrame = true;
                    try {
                        byte2image(frameData,"sdcard/一帧.png");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }
    public static void saveToSDCard(String filePath, String filecontent) throws Exception {
        File file = new File(filePath);
        FileOutputStream outStream = new FileOutputStream(file);
        outStream.write(filecontent.getBytes());
        outStream.close();
    }
    public void byte2image(byte[] data,String path){
        if(data.length<3||path.equals("")) return;
        try{
            FileOutputStream imageOutput = new FileOutputStream(new File(path));
            imageOutput.write(data, 0, data.length);
            imageOutput.close();
            System.out.println("Make Picture success,Please find image in " + path);
        } catch(Exception ex) {
            System.out.println("Exception: " + ex);
            ex.printStackTrace();
        }
    }
    @Override
    protected void onDestroy() {
        aacPlayer.stop();
        player.release();
        yuvSaver.setRunning(false);
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        CameraUtils.setTranslucentMode(this);
        super.onResume();
    }
}

