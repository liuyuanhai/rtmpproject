package easing.android.media.RtmpPlayer.util;

import android.util.Log;

public class LogUtil {

    public static void log(Object... args) {
        String log = "";
        for (Object arg : args)
            log = log + "  " + arg;
        Log.e("LogUtil", log);
    }
}
